import sys
import os
import unittest
sys.path.append(os.path.join(os.path.dirname(__file__), '../../src/'))
import calc

class CalcTest(unittest.TestCase):
    def setUp(self):
        # initialize
        pass
    
    def tearDown(self):
        # finish
        pass
    
    def test_normal(self):
        self.assertEqual(3, calc.calc(1, 2))
        
if __name__ == "__main__":
    unittest.main()